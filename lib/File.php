<?php
namespace Lib;
class File{
    function setFile($file){
        $this->file = $file;
        return $this;
    }

    function read(){
        return file_get_contents($this->file);
    }

    function write($contents, $append = false){
        if(is_file($this->file) && is_writable($this->file)){
            $option = null;
            if($append){
                $option = FILE_APPEND;
            }
            return file_put_contents($this->file, $contents, $option);
        }
        return false;
    }

    function chown($username){
        return chown($this->file, $username);
    }

    function chmod($perm){
        return chmod($this->file, $perm);
    }
}