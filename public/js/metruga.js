(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("Metruga", [], factory);
	else if(typeof exports === 'object')
		exports["Metruga"] = factory();
	else
		root["Metruga"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _Xhr = __webpack_require__(1);
	
	var _Xhr2 = _interopRequireDefault(_Xhr);
	
	var _Navigation = __webpack_require__(2);
	
	var _Navigation2 = _interopRequireDefault(_Navigation);
	
	var _Socket = __webpack_require__(4);
	
	var _Socket2 = _interopRequireDefault(_Socket);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Metruga = function () {
	  function Metruga() {
	    _classCallCheck(this, Metruga);
	
	    this._name = 'Metruga';
	    this.navigation = new _Navigation2.default(this);
	    this.inspect = window.console;
	    this.fn = this;
	  }
	
	  _createClass(Metruga, [{
	    key: 'xhr',
	    value: function xhr(options) {
	      return new _Xhr2.default(options);
	    }
	  }, {
	    key: 'extend',
	    value: function extend(obj, name) {
	      for (var i in obj) {
	        if (obj.hasOwnProperty(i)) {
	          if (name !== undefined) {
	            this[name][i] = obj[i];
	          } else {
	            this[i] = obj[i];
	          }
	        }
	      }
	      return this;
	    }
	  }, {
	    key: 'date',
	    value: function date(params) {
	      if (params) {
	        return new Date(params);
	      }
	      return new Date();
	    }
	  }, {
	    key: 'map',
	    value: function map(callback) {
	      var results = [],
	          i = 0;
	      for (; i < this.element.length; i++) {
	        results.push(callback.call(this.element, this.element[i], i));
	      }
	      return results;
	    }
	  }, {
	    key: 'mapOne',
	    value: function mapOne(callback) {
	      var m = this.map(callback);
	      return m.length > 1 ? m : m[0];
	    }
	  }, {
	    key: 'forEach',
	    value: function forEach(callback) {
	      this.map(callback);
	      return this;
	    }
	  }, {
	    key: 'html',
	    value: function html(_html) {
	      if (typeof _html !== 'undefined') {
	        this.forEach(function (el) {
	          el.innerHTML = _html;
	        });
	        return this;
	      }
	      return this.mapOne(function (el) {
	        return el.innerHTML;
	      });
	    }
	  }, {
	    key: 'addClass',
	    value: function addClass(className, options) {
	      var $top = this;
	      if (typeof className !== 'undefined') {
	        this.forEach(function (el) {
	          if (!$top.hasClass(el, className)) {
	            el.className += ' ' + className;
	          }
	          if ((typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object') {
	            $top.createClass('.' + className, options);
	          }
	        });
	        return this;
	      }
	      return this.mapOne(function (el) {
	        return el.className;
	      });
	    }
	  }, {
	    key: 'removeClass',
	    value: function removeClass(className) {
	      var $top = this;
	      if (typeof className !== 'undefined') {
	        this.forEach(function (el) {
	          if ($top.hasClass(el, className)) {
	            el.className = el.className.replace(new RegExp('(?:^|\\s)' + className + '(?!\\S)'), '');
	          }
	        });
	        return this;
	      }
	      return this.mapOne(function (el) {
	        return el.className;
	      });
	    }
	  }, {
	    key: 'createClass',
	    value: function createClass(name, rules) {
	      var style = document.createElement('style');
	      style.type = 'text/css';
	      var content = name + '{';
	      for (var i in rules) {
	        if (rules.hasOwnProperty(i)) {
	          content += i + ':' + rules[i] + ';';
	        }
	      }
	      content += '}';
	      style.innerText = content;
	      document.getElementsByTagName('head')[0].appendChild(style);
	      return this;
	    }
	  }, {
	    key: 'hasClass',
	    value: function hasClass(element, cls) {
	      return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
	    }
	  }, {
	    key: 'select',
	    value: function select(selector) {
	      if (selector !== undefined) {
	        if (typeof selector === 'string') {
	          this.element = document.querySelectorAll(selector);
	        } else if (selector.length) {
	          this.element = selector;
	        } else {
	          this.element = [selector];
	        }
	      }
	      return this;
	    }
	  }, {
	    key: 'socket',
	    value: function socket(url, options) {
	      return new _Socket2.default(url, options);
	    }
	  }, {
	    key: 'info',
	    value: function info(message) {
	      if (_typeof(window.console) === 'object') {
	        window.console.info(message);
	      }
	    }
	  }, {
	    key: 'log',
	    value: function log(message) {
	      if (_typeof(window.console) === 'object') {
	        window.console.log(message);
	      }
	    }
	  }, {
	    key: 'error',
	    value: function error(_error) {
	      if (_typeof(window.console) === 'object') {
	        window.console.error(_error);
	      }
	    }
	  }, {
	    key: 'getObjectClass',
	    value: function getObjectClass(obj) {
	      if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) !== 'object' || obj === null) {
	        return false;
	      }
	      return (/(\w+)\(/.exec(obj.constructor.toString())[1]
	      );
	    }
	  }, {
	    key: 'scriptLoader',
	    value: function scriptLoader(urls) {
	      for (var i in urls) {
	        var script = document.createElement('script');
	        script.type = 'text/javascript';
	        script.src = urls[i];
	        document.getElementsByTagName('head')[0].appendChild(script);
	      }
	    }
	  }, {
	    key: 'render',
	    value: function render(template, params) {
	      return this.navigation.render(template, params);
	    }
	  }, {
	    key: 'name',
	    get: function get() {
	      return this._name;
	    }
	  }]);
	
	  return Metruga;
	}();
	
	exports.default = Metruga;
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Xhr = function () {
	  function Xhr(opts) {
	    _classCallCheck(this, Xhr);
	
	    this.events = {
	      READY_STATE_CHANGE: 'readystatechange',
	      LOAD_START: 'loadstart',
	      PROGRESS: 'progress',
	      ABORT: 'abort',
	      ERROR: 'error',
	      LOAD: 'load',
	      TIMEOUT: 'timeout',
	      LOAD_END: 'loadend'
	    };
	    this.opts = opts;
	  }
	  /**
	   * Send
	   * @param url - URL to request
	   * @param callback
	   * @param method
	   * @param data
	   * @param sync
	   */
	
	
	  _createClass(Xhr, [{
	    key: 'send',
	    value: function send(url, method, data) {
	      var _this = this;
	
	      return new Promise(function (resolve, reject) {
	        var xhr = new XMLHttpRequest();
	        var m = method || 'GET';
	
	        xhr.open(m, url);
	        // Set headers
	        xhr.setRequestHeader('Content-Type', _this.opts.contentType || 'application/json');
	        // Custom
	        if (_this.opts.headers) {
	          for (var name in _this.opts.headers) {
	            var value = _this.opts.headers[name];
	
	            xhr.setRequestHeader(name, value);
	          }
	        }
	        // Transmit credentials
	        if (_this.opts.withCredentials) xhr.withCredentials = true;
	        data = data ? _this.parseData(data) : null;
	
	        xhr.addEventListener(_this.events.LOAD, function () {
	          // ==0 for files.
	          if (xhr.status >= 200 && xhr.status < 300 || xhr.status === 0) {
	
	            var responseText = '';
	            if (xhr.responseText) {
	              responseText = _this.opts.json ? JSON.parse(xhr.responseText) : xhr.responseText;
	            }
	            resolve(responseText, xhr);
	          } else {
	            reject(_this.reject(xhr));
	          }
	        });
	        // Handle basic events
	        xhr.addEventListener(_this.events.ABORT, function () {
	          return reject(_this.reject(xhr));
	        });
	        xhr.addEventListener(_this.events.ERROR, function () {
	          return reject(_this.reject(xhr));
	        });
	        xhr.addEventListener(_this.events.TIMEOUT, function () {
	          return reject(_this.reject(xhr));
	        });
	
	        data ? xhr.send(data) : xhr.send();
	      });
	    }
	    /**
	     * Reject handler.
	     * @param xhr
	     * @returns {string}
	     */
	
	  }, {
	    key: 'reject',
	    value: function reject(xhr) {
	      var responseText = '';
	      if (xhr.responseText) {
	        responseText = this.opts.json ? JSON.parse(xhr.responseText) : xhr.responseText;
	      }
	      return responseText;
	    }
	    /**
	     * Create query string
	     * @param data
	     * @returns {Array}
	     */
	
	  }, {
	    key: 'parseData',
	    value: function parseData(data) {
	      // JSON
	      if (this.opts.contentType === 'application/json') return JSON.stringify(data);
	      // Query string
	      var query = [];
	      if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)).toLowerCase() === 'string' || (typeof data === 'undefined' ? 'undefined' : _typeof(data)).toLowerCase() === 'number') {
	        query.push(data);
	      } else {
	        for (var key in data) {
	          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	        }
	      }
	
	      return query.join('&');
	    }
	    /**
	     * GET Wrapper
	     * @param url
	     * @returns {*}
	     */
	
	  }, {
	    key: 'get',
	    value: function get(url) {
	      return this.send(url);
	    }
	    /**
	     * POST Wrapper
	     * @param url
	     * @param data
	     * @returns {*}
	     */
	
	  }, {
	    key: 'post',
	    value: function post(url, data) {
	      return this.send(url, 'POST', data);
	    }
	    /**
	     * DELETE Wrapper
	     * @param url
	     * @returns {*}
	     */
	
	  }, {
	    key: 'delete',
	    value: function _delete(url) {
	      return this.send(url, 'DELETE');
	    }
	    /**
	     * PUT Wrapper
	     * @param url
	     * @param data
	     * @returns {*}
	     */
	
	  }, {
	    key: 'put',
	    value: function put(url, data) {
	      return this.send(url, 'PUT', data);
	    }
	  }]);
	
	  return Xhr;
	}();
	
	exports.default = Xhr;
	module.exports = exports['default'];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _micromustache = __webpack_require__(3);
	
	var _micromustache2 = _interopRequireDefault(_micromustache);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Navigation = function () {
	  function Navigation(metruga) {
	    _classCallCheck(this, Navigation);
	
	    this.__navigation = null;
	    this.__root = null;
	    this.__error = null;
	    this.__metruga = metruga;
	  }
	
	  _createClass(Navigation, [{
	    key: 'path',
	    value: function path() {
	      return window.location.hash.split('#')[1];
	    }
	  }, {
	    key: 'hashToArray',
	    value: function hashToArray() {
	      return this.path().split('/');
	    }
	  }, {
	    key: 'hashChange',
	    value: function hashChange(func) {
	      return window.addEventListener('hashchange', function (e) {
	        if (typeof func === 'function') {
	          func();
	        }
	      }, false);
	    }
	  }, {
	    key: 'render',
	    value: function render(template, params) {
	      var temp = this.__metruga.select(template).html();
	      // Mustache.parse(temp);
	      var rendered = _micromustache2.default.render(temp, params);
	      this.__metruga.select(template).html(rendered);
	    }
	  }, {
	    key: 'run',
	    value: function run() {
	      var _nav = this.__navigation;
	      var _root = this.__root;
	      var template, params, func, urlPath;
	
	      if ('onhashchange' in window) {
	        window.onhashchange = function () {
	          urlPath = window.location.hash.split('#')[1];
	          if ((typeof _nav === 'undefined' ? 'undefined' : _typeof(_nav)) === 'object' && urlPath !== undefined) {
	            for (var i in _nav) {
	              if (urlPath === _nav[i].path) {
	                if (typeof _nav[i].fn === 'function') {
	                  func = _nav[i].fn;
	                  func();
	                }
	                if (_typeof(_nav[i].render) === 'object') {
	                  template = _nav[i].render.template;
	                  params = _nav[i].render.template;
	
	                  this.render(template, params);
	                }
	              }
	            }
	          } else {
	            if (typeof _root.fn === 'function') {
	              func = _root.fn;
	              func();
	            }
	          }
	        };
	      }
	    }
	  }, {
	    key: 'config',
	    value: function config(selector, nav, options) {
	      this.__navigation = nav;
	      this.__selector = this.__metruga.select(selector);
	      this.options = this.__metruga.extend(options);
	
	      /* TODO: Make conf generator */
	
	      this.__navigation = nav;
	      var selected = this.__metruga.select(selector);
	      this.options = options;
	
	      if (_typeof(this.options.root) === 'object') {
	        this.__root = this.options.root;
	      }
	      var parent = document.createElement('ul');
	      if (typeof this.options.parent === 'string') {
	        parent = document.createElement(this.options.parent);
	      }
	      if (typeof this.options.class === 'string') {
	        parent.className = this.options.class;
	      }
	      if (typeof this.options.id === 'string') {
	        parent.id = this.options.id;
	      }
	      if ((typeof nav === 'undefined' ? 'undefined' : _typeof(nav)) !== 'object') {} else {
	        for (var element in nav) {
	          var child = document.createElement('li');
	          if (typeof this.options.child === 'string') {
	            child = document.createElement(this.options.child);
	          }
	          if (typeof nav[element].class === 'string') {
	            child.className = nav[element].class;
	          }
	          if (typeof nav[element].id === 'string') {
	            child.id = nav[element].id;
	          }
	          if (_typeof(nav[element].link) === 'object') {
	            var a = document.createElement('a');
	            a.href = '#' + nav[element].path;
	            if (typeof nav[element].link.class === 'string') {
	              a.className = nav[element].link.class;
	            }
	            if (typeof nav[element].link.id === 'string') {
	              a.id = nav[element].link.id;
	            }
	            if (typeof nav[element].link.text === 'string') {
	              a.innerHTML = nav[element].link.text;
	            }
	            child.innerHTML = a.outerHTML;
	          } else {
	            if (typeof nav[element].content === 'string') {
	              child.href = '#' + nav[element].path;
	              child.innerHTML = nav[element].content;
	            }
	          }
	          parent.innerHTML = parent.innerHTML + child.outerHTML;
	        }
	
	        selected.html(parent.outerHTML);
	
	        this.run();
	      }
	    }
	  }]);
	
	  return Navigation;
	}();
	
	exports.default = Navigation;
	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	!function(e){if(true)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var n;n="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,n.micromustache=e()}}(function(){function e(e,n,o){return"string"!=typeof e?e:("function"==typeof n&&"undefined"==typeof o&&(o=n),"object"==typeof n&&null!==n||(n={}),e.replace(/\{\{\s*(.*?)\s*\}\}/g,function(e,r){function f(e,o){if(null===e)return"";var r=i[o],u=e[r],c=typeof u;return"function"===c?t(u.call(n,r,e,i,o)):"object"===c?(o++,o<i.length?f(u,o):t(u)):t(u)}if(o){var u=o(n,r);if("undefined"!=typeof u)return t(u)}var i=r.split(".");return f(n,0)}))}function n(n,t){return function(o){return e(n,o,t)}}function t(e){switch(typeof e){case"string":case"number":case"boolean":return e;case"object":if(null===e)return"";try{return JSON.stringify(e)}catch(e){return"{...}"}return e?toJsonPolitely(e):"";default:return""}}var o,r;return o={exports:r={}},r.to_html=r.render=e,r.compile=n,o.exports});

/***/ },
/* 4 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	/**
	 * Created by oytun on 17/10/2016.
	 */
	var Socket = function () {
	  function Socket(url, options) {
	    _classCallCheck(this, Socket);
	
	    this.inspect = window.console;
	    this.error = this.inspect.error;
	    this.msg = this.inspect.msg;
	
	    var _url, _options, _open, _message, _close;
	    _url = url;
	    _options = this.extend(options);
	
	    if ('WebSocket' in window) {
	      if (typeof _url === 'string') {
	        this._socket = new WebSocket(_url);
	
	        this._socket.onopen = function (event) {
	          if (typeof _options.open === 'function') {
	            _open = options.open;
	            _open(event);
	          }
	        };
	
	        this._socket.onmessage = function (event) {
	          if (typeof _options.message === 'function') {
	            _message = options.message;
	            _message(event);
	          }
	        };
	
	        this._socket.onclose = function (event) {
	          if (typeof _options.close === 'function') {
	            _close = options.close;
	            _close(event);
	          }
	        };
	      } else {
	        this.inspect.error('Your url doesn\'t string.');
	      }
	    } else {
	      this.inspect.error('Your browser doesn\'t support this function.');
	    }
	
	    return this._socket;
	  }
	
	  _createClass(Socket, [{
	    key: 'send',
	    value: function send(message) {
	      return this._socket.send(message);
	    }
	  }, {
	    key: 'extend',
	    value: function extend(obj, name) {
	      for (var i in obj) {
	        if (obj.hasOwnProperty(i)) {
	          if (name !== undefined) {
	            this[name][i] = obj[i];
	          } else {
	            this[i] = obj[i];
	          }
	        }
	      }
	      return this;
	    }
	  }]);
	
	  return Socket;
	}();
	
	exports.default = Socket;
	module.exports = exports['default'];

/***/ }
/******/ ])
});
;
//# sourceMappingURL=Metruga.js.map